tasks = []


// GENEREATE NEW TASK
const gen = () => {
    let inner_html = ""
    tasks.forEach((element, index) => {
        inner_html += `<li><input type="checkbox" onClick="checkTask(this)" data-index="${parseInt(index)}" ${element.isDone ? "checked" : ""}/> ${index + 1}.) ${element.name} | ${element.responsible} <button onClick="deleteTask(this)">X</button>  </li>`;
    });
    return inner_html;
};


// SET THE VALUE OF THE LIST TO THE TASKS
const get_tasks = () => {
    document.getElementById("taskList").innerHTML = gen();
}


// CHANGE THE VALUE IN THE OBJ
const checkTask = (elem) => {
    tasks[elem.attributes['data-index'].value].isDone = elem.checked
    get_tasks()
}


// DELETE THE VALUE IN THE OBJ
const deleteTask = (elem) => {
    tasks.splice([elem.previousElementSibling.attributes['data-index'].value], 1)
    get_tasks()
}


// INITIAL CALL TO LOAD THE TASKS (not neccessary if we save the tasks in js because its empty at the start)
get_tasks()


// EVENT LISTENER ON BUTTON ONCLICK
document.getElementById("addTask").addEventListener("click", () => {
    task_name = document.getElementById("taskMission").value
    task_responsible = document.getElementById("taskResponsible").value
    if (task_name && task_responsible) {
        tasks.push({ name: task_name, responsible: task_responsible, isDone: false })
        document.getElementById("taskMission").value = task_responsible = document.getElementById("taskResponsible").value = ""
        console.log(`"${task_name}" added to task list!`)
        get_tasks()
    }
    else if (!task_name) {
        alert("Please enter a task name!")
    }
    else if (!task_responsible) {
        alert("Please enter a task responsibility!")
    }
});


// EVENT LISTENER ON BUTTON ONCLICK
document.getElementById("deleteTasks").addEventListener("click", () => {
    tasks = []
    console.log('Cleared Task List!')
    get_tasks()

});